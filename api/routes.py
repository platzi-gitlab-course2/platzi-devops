from flask import jsonify
from flask import Blueprint


api_bp = Blueprint('api', __name__)


@api_bp.route('/')
def index():
    return jsonify('Hello World from API')


@api_bp.route('/hello')
def hello():
    return jsonify('Hello from Flask Jenkins Tests :)')

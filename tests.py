import unittest
import json
from flask import current_app
from api import create_app


class APITestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def tearDown(self):
        self.app_context.pop()

    def test_app(self):
        assert self.app is not None
        assert current_app == self.app

    def test_index(self):
        response = self.client.get('api/')
        assert response.status_code == 200
        assert response.json == 'Hello World from API'

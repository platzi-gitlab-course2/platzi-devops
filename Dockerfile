FROM python:3.7-alpine

RUN adduser -D dev

WORKDIR /home/dev

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY api api
COPY config.py ./
COPY app.py ./
COPY tests.py ./
COPY boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP app.py

RUN chown -R dev:dev ./
USER dev

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
